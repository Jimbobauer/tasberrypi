### simple starting point to just send an 'l' to the PSI board and then listen to and
### log the LOG data


import serial
import time
import csv
import os

ser = serial.Serial(port='/dev/ttyUSB0',
               baudrate = 57600,
               parity=serial.PARITY_NONE,
               stopbits=serial.STOPBITS_ONE,
               bytesize=serial.EIGHTBITS,
               timeout=1)

ser.flushInput()

time.sleep(2) #wait a couple of seconds for the PSI board to finishing sending its wake up string

print("Enable Logging on PSI Board")
ser.write(('l'+'\r\n').encode('utf-8'))

time.sleep(1) #wait a second for the PSI board to let us know logging is on
ser.flushInput()

while True:
    try:
        ser_bytes = ser.readline()
        decoded_bytes = ser_bytes[0:len(ser_bytes)-2].decode("utf-8")
        dataArray = decoded_bytes.split(',')
#        print(dataArray)
        os.system('clear')
        print('Concentration = ', int(dataArray[0],16), 'ppm.m')
        print('System Error = ', dataArray[1])
        print('System Warning = ', dataArray[2])
        print('Battery Voltage = ', int(dataArray[3],16)/305 ,'Vdc' )
        print('F1 Signal Amplitude = ', dataArray[4])
        print('F2 Signal Amplitude = ', dataArray[5])
        print('Preamp DC Signal Amplitude = ', dataArray[6])
        print('Vtherm Monitor = ', dataArray[7])
        print('Unit Serial Number = ', dataArray[8])
        print('DC Modulation Monitor = ', dataArray[9])
        print('Concentration Threshold = ', dataArray[10])
        print('Laser DC Current Ampl. Monitor = ', dataArray[11])
        print('F2 I Value = ', dataArray[12])
        print('F2 I Value = ', dataArray[13])
        print('DMD Results = ', dataArray[14])

        with open("test_data.csv","a") as f:
            writer = csv.writer(f,delimiter=",")
#            writer.writerow([time.time(),decoded_bytes])
            writer.writerow([time.time(),dataArray])
    except:
        print("Keyboard Interrupt")
        break
