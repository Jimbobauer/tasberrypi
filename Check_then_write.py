# Learned it here...
# https://www.devdungeon.com/content/gui-programming-python
# https://pyinmyeye.blogspot.com/2012/08/tkinter-menubutton-demo.html

# first import message box stuff
import tkinter
from tkinter import Tk, Label, Y, LEFT, RIGHT, Button
from tkinter import messagebox as mbox


########################################################################
root0 = Tk()

screen_width = root0.winfo_screenwidth()
screen_height = root0.winfo_screenheight()

#print("Screen width:", screen_width)
#print("Screen height:", screen_height)

#Center on screen

w1 = 300 # width for the Tk root
h1 = 100 # height for the Tk root

# get screen width and height
ws1 = root0.winfo_screenwidth()
hs1 = root0.winfo_screenheight()

# calculate x and y coordinates for the Tk root window
x1 = (ws1/2) - (w1/2)
y1 = (hs1/2) - (h1/2)

# set the dimensions of the screen 
# and where it is placed
root0.geometry('%dx%d+%d+%d' % (w1, h1, x1, y1))

# Title tk box
root0.title("Power The Board On")

# Create a label as a child of root window
my_text = Label(root0,justify=LEFT, text= 'Make Sure The PSI Board Is Powered ON.')

my_text.config(font=('helvetica', 10))
my_text.pack(padx=5, pady=5)

# continue and leave open
continue0_button = Button(root0, text='Continue', command=root0.destroy)
#continue0_button = Button(root, text='Continue', command=okay)
continue0_button.pack()

root0.mainloop()
##########################################################################
root3 = Tk()

screen_width = root3.winfo_screenwidth()
screen_height = root3.winfo_screenheight()

#print("Screen width:", screen_width)
#print("Screen height:", screen_height)

#Center on screen

w1 = 300 # width for the Tk root
h1 = 100 # height for the Tk root

# get screen width and height
ws1 = root3.winfo_screenwidth()
hs1 = root3.winfo_screenheight()

# calculate x and y coordinates for the Tk root window
x1 = (ws1/2) - (w1/2)
y1 = (hs1/2) - (h1/2)

# set the dimensions of the screen 
# and where it is placed
root3.geometry('%dx%d+%d+%d' % (w1, h1, x1, y1))

# Title tk box
root3.title("Update The Values.")

# Create a label as a child of root window
my_text = Label(root3,justify=LEFT, text= 'Make Sure The Values on "values.txt" Are Correct.')

my_text.config(font=('helvetica', 10))
my_text.pack(padx=5, pady=5)

# continue and leave open
continue0_button = Button(root3, text='Continue', command=root3.destroy)
#continue0_button = Button(root, text='Continue', command=okay)
continue0_button.pack()

root3.mainloop()


############################################################################
# then pull data from text document "values.txt"

#OPEN FILE TO BE READ
myFile = open('values.txt', 'r')
listoflines = myFile.read().splitlines()

#Laser DC Setpoint read from values.txt
aline = listoflines[0]
lineitems = aline.split('=')
ldcs = lineitems[1]
#print("Laser DC Setpoint")
#print (ldcs)

#Laser temperature setpoint read from values.txt
aline1 = listoflines[1]
lineitems1 = aline1.split('=')
lts = lineitems1[1]
#print ("Laser Temperature")
#print (lts)

#Laser AC setpoint read from values.txt
aline2 = listoflines[2]
lineitems2 = aline2.split('=')
lacs = lineitems2[1]
#print ("Laser AC setpoint")
#print (lacs)

#Laser Tuning Rate read from values.txt
aline3 = listoflines[3]
lineitems3 = aline3.split('=')
ltr = lineitems3[1]
#print ("Laser Tuning Rate")
#print (ltr)

#Laser Calibration Constant read from values.txt
aline4 = listoflines[4]
lineitems4 = aline4.split('=')
lcc = lineitems4[1]
#print ("Laser Calibration Constant")
#print (lcc)
#
#Laser Temperature DAC Offset read from values.txt
aline5 = listoflines[5]
lineitems5 = aline5.split('=')
ltdo = lineitems5[1]
#print ("Laser Temperature DAC Offset")
#print (ltdo)

#Concentration Offset read from values.txt
aline6 = listoflines[6]
lineitems6 = aline6.split('=')
co = lineitems6[1]
#print ("Concentration Offset")
#print (co)

#Photo Current Monitor Threshold read from values.txt
aline7 = listoflines[7]
lineitems7 = aline7.split('=')
pcmt = lineitems7[1]
#print ("Photo Current Monitor Threshold")
#print (pcmt)

#Current Shutdown Record read from values.txt
aline8 = listoflines[8]
lineitems8 = aline8.split('=')
csr = lineitems8[1]
#print ("Current Shutdown Record")
#print (csr)

#Serial Number read from values.txt
aline9 = listoflines[9]
lineitems9 = aline9.split('=')
sn = lineitems9[1]
#print ("Serial Number")
#print (sn)

#Modulator Gain read from values.txt
aline10 = listoflines[10]
lineitems10 = aline10.split('=')
mg = lineitems10[1]
#print ("Modulator Gain")
#print (mg)

#F2 Correction for I read from values.txt
aline11 = listoflines[11]
lineitems11 = aline11.split('=')
f2cfi = lineitems11[1]
#print ("F2 Modulator for I")
#print (f2cfi)

#F2 Correction for Q read from values.txt
aline12 = listoflines[12]
lineitems12 = aline12.split('=')
f2cfq = lineitems12[1]
#print ("F2 Modulator for Q")
#print (f2cfq)

#Minimum F1 read from values.txt
aline13 = listoflines[13]
lineitems13 = aline13.split('=')
mf1 = lineitems13[1]
#print ("Minimum F1")
#print (mf1)

#Minimum F2 read from values.txt
aline14 = listoflines[14]
lineitems14 = aline14.split('=')
mf2 = lineitems14[1]
#print ("Minimum F2")
#print (mf2)

#Startup Lockout read from values.txt
aline15 = listoflines[15]
lineitems15 = aline15.split('=')
sl = lineitems15[1]
#print ("Startup Lockout")
#print (sl)

##########################################################################################
# Read existing values on PSI Board
# mostly taken from J.Bower Ser_Interface_PSI.py


import serial
import time
import csv
import os


def initialize_serialport():
    global ser
    ser = serial.Serial(port='/dev/ttyUSB0',
               baudrate = 57600,
               parity=serial.PARITY_NONE,
               stopbits=serial.STOPBITS_ONE,
               bytesize=serial.EIGHTBITS,
               timeout=1)

### routine to write a string to PSI board
def write_to_PSI(sendCommand):
    ser.write((sendCommand).encode('utf-8')) #removed CR LF as PSI board doesnt seem to need them`

### routine to read a line (up to CR LF) from PSI board
### need to figure out how to deal wht the extra '>' prompt character that 
###   gets sent for some responses
def listen_to_PSI():
    ser_bytes = ser.readline()
    decoded_bytes = ser_bytes[0:len(ser_bytes)-2].decode('utf-8')
    return decoded_bytes

### routine to turn logging mode ON if OFF 
def turnLoggingON():
    print("turning on logging")
    write_to_PSI('l')
    output = listen_to_PSI()
    if output == 'Log Off':
        print("logging was already on... turning it back on")
        write_to_PSI('l')

### routine to turn logging mode OFF if ON 
def turnLoggingOFF():
    print("turning off logging")
    write_to_PSI('l')
    output = listen_to_PSI()
    if output == 'Log On':
        print("logging was already off... turning it back off")
        write_to_PSI('l')

### routine to turn debug mode ON if OFF 
def turnDebugON():
    print("turning on debug")
    write_to_PSI('o')
    output = listen_to_PSI()
    print("debug on:", output)
    goofything = ser.read() # just read a character to get rid of the '>'
#    print(goofything)
    time.sleep(0.1)
    if output == 'Debug Off':
        print("Debug was already on... turning it back on")
        write_to_PSI('o')
        output = listen_to_PSI()
        print("debug on:", output)
        goofything = ser.read() # just read a character to get rid of the '>'

### routine to read contents of flash page
### ASSUMES PSI is in Debug Mode ON
def readFlashPage(page):
 #   print("reading page", page)
    write_to_PSI('f')
    output = listen_to_PSI()
    if output == 'Page To Read:':
        write_to_PSI(page)
        flash = listen_to_PSI() #PSI echos back the input
  #      print('flash =',flash)
        heading = listen_to_PSI() # the header CONTENTS:
 #       print(heading)
        contents = listen_to_PSI() # the actual flash page contents
 #       print(contents)
        goofything = ser.read() # just read a character to get rid of the '>'
#        print(goofything)
        list.append(contents)
        print(flash + " = " + contents)
        

### create a list
list=[]

### here is where the main loop starts
def main():
    initialize_serialport()
    ser.reset_input_buffer()
 #   turnLoggingOFF() # make sure logging is not running
    turnDebugON()
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('00')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('01')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('02')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('03')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('04')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('05')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('06')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('07')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('08')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('09')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('0A')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('0B')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('0C')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('0D')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('0E')
    
    time.sleep(0.5) # just to let the PSI board catch its breath
    readFlashPage('0F')
    
    print(list)
    
 #   turnDebugOFF()
    ser.close()

# python bit to figure how who started this
if __name__ == "__main__":
    main()


###############################################################################

# Display current existing PSI Board Settings.

root = Tk()

screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

#print("Screen width:", screen_width)
#print("Screen height:", screen_height)

#Center on screen

w1 = 350 # width for the Tk root
h1 = 550 # height for the Tk root

# get screen width and height
ws1 = root.winfo_screenwidth()
hs1 = root.winfo_screenheight()

# calculate x and y coordinates for the Tk root window
x1 = (ws1/2) - (w1/2)
y1 = (hs1/2) - (h1/2)

# set the dimensions of the screen 
# and where it is placed
root.geometry('%dx%d+%d+%d' % (w1, h1, x1, y1))

# Title tk box
root.title("EXISTING Values on PSI Board")

# Create a label as a child of root window
my_text = Label(root,justify=LEFT, text=
                
                'Laser DC Setpoint = '+ list[0]
              + '\n\nLaser temperature setpoint = '+ list[1]
              + '\n\nLaser AC setpoint = '+ list[2]
	      + '\n\nLaser Tuning Rate = '+ list[3]
              + '\n\nLaser Calibration Constant = '+ list[4]
	      + '\n\nLaser Temperature DAC Offset = '+ list[5]
	      + '\n\nConcentration Offset = '+ list[6]
	      + '\n\nPhoto Current Monitor Threshold = '+ list[7]
	      + '\n\nCurrent Shutdown Record = '+ list[8]
	      + '\n\nSerial Number = '+ list[9]
	      + '\n\nModulator Gain = '+ list[10]
	      + '\n\nF2 Correction for I = '+ list[11]
	      + '\n\nF2 Correction for Q = '+ list[12]
	      + '\n\nMinimum F1 = '+ list[13]
	      + '\n\nMinimum F2 = '+ list[14]
	      + '\n\nStartup Lockout = '+ list[15])

my_text.config(font=('helvetica', 10))
my_text.pack(padx=5, pady=5)

#def okay():
 #   root.quit()

# continue and leave open
continue0_button = Button(root, text='Continue', command=root.destroy)
#continue0_button = Button(root, text='Continue', command=okay)
continue0_button.pack()

root.mainloop()



##################################################################################


#now display data to be WRITTEN to board with option to continue or cancel.

root2 = Tk()

screen_width = root2.winfo_screenwidth()
screen_height = root2.winfo_screenheight()

#print("Screen width:", screen_width)
#print("Screen height:", screen_height)

#Center on screen

w = 350 # width for the Tk root
h = 550 # height for the Tk root

# get screen width and height
ws = root2.winfo_screenwidth()
hs = root2.winfo_screenheight()

# calculate x and y coordinates for the Tk root window
x = (ws/2) - (w/2)
y = (hs/2) - (h/2)

# set the dimensions of the screen 
# and where it is placed
root2.geometry('%dx%d+%d+%d' % (w, h, x, y))

# Title tk box
root2.title("Values to Write to PSI Board")

# Create a label as a child of root window
my_text2 = Label(root2,justify=LEFT, text=
                
                'Laser DC Setpoint = '+ ldcs
              + '\n\nLaser temperature setpoint = '+ lts
              + '\n\nLaser AC setpoint = '+ lacs
	      + '\n\nLaser Tuning Rate = '+ ltr
              + '\n\nLaser Calibration Constant = '+ lcc
	      + '\n\nLaser Temperature DAC Offset = '+ ltdo
	      + '\n\nConcentration Offset = '+ co
	      + '\n\nPhoto Current Monitor Threshold = '+ pcmt
	      + '\n\nCurrent Shutdown Record = '+ csr
	      + '\n\nSerial Number = '+ sn
	      + '\n\nModulator Gain = '+ mg
	      + '\n\nF2 Correction for I = '+ f2cfi
	      + '\n\nF2 Correction for Q = '+ f2cfq
	      + '\n\nMinimum F1 = '+ mf1
	      + '\n\nMinimum F2 = '+ mf2
	      + '\n\nStartup Lockout = '+ sl)

#my_text.config(font=('times', 10))
my_text2.config(font=('helvetica', 10))
my_text2.pack(padx=5, pady=5)


# continue writing values to PSI board
continue_button = Button(root2, text='Continue Writing Values to PSI Board', command=root2.destroy)
continue_button.pack()

# if CANCEL, kill program.
Cancel_button = Button(root2, text='Stop to change values', command=quit)
Cancel_button.pack()

root2.mainloop()


####################################################################################

# Write new values to PSI Board.

### routine to write contents to f flash page
### ASSUMES PSI is in Debug Mode ON
def writeFlashPage(page,value):
    print("Writing page", page, 'to', value)
    write_to_PSI('F')
    output = listen_to_PSI()
  #  print(output)
    time.sleep(.1)
    if output == 'Page To Write:':
   #     print('pick page')
        write_to_PSI(page)
        output = listen_to_PSI()
        output = listen_to_PSI()
  #      print(output)
        if output == 'Enter New Value:':
                write_to_PSI(value)
                output = listen_to_PSI()
                output = listen_to_PSI()
#                print(output)
                
##### here is where the main loop starts
def main():
    initialize_serialport()
    ser.reset_input_buffer()
    turnDebugON()
    time.sleep(0.5) # just to let the PSI board catch its breath
    writeFlashPage('00',ldcs)
    time.sleep(.5)
    writeFlashPage('01',lts)
    time.sleep(.5)
    writeFlashPage('02',lacs)
    time.sleep(.5)
    writeFlashPage('03',ltr)
    time.sleep(.5)
    writeFlashPage('04',lcc)
    time.sleep(.5)
    writeFlashPage('05',ltdo)
    time.sleep(.5)
    writeFlashPage('06',co)
    time.sleep(.5)
    writeFlashPage('07',pcmt)
    time.sleep(.5)
    writeFlashPage('08',csr)
    time.sleep(.5)
    writeFlashPage('09',sn)
    time.sleep(.5)
    writeFlashPage('0A',mg)
    time.sleep(.5)
    writeFlashPage('0B',f2cfi)
    time.sleep(.5)
    writeFlashPage('0C',f2cfq)
    time.sleep(.5)
    writeFlashPage('0D',mf1)
    time.sleep(.5)
    writeFlashPage('0E',mf2)
    time.sleep(.5)
    writeFlashPage('0F',sl)
    time.sleep(.5)
    write_to_PSI('o')
    ser.close()

# python bit to figure how who started this
if __name__ == "__main__":
        main()
        
window = tkinter.Tk()
window.wm_withdraw()        
mbox.showinfo("Done", "Write Complete")

print("wrote it")

