# write the serial number to the PSI Board.


import serial
import time
import csv
import os

#OPEN FILE TO BE READ
myFile = open('values.txt', 'r')
listoflines = myFile.read().splitlines()

#Serial Number read from values.txt
aline9 = listoflines[9]
lineitems9 = aline9.split('=')
sn = lineitems9[1]
#print ("Serial Number")
#print (sn)

def initialize_serialport():
    global ser
    ser = serial.Serial(port='/dev/ttyUSB0',
               baudrate = 57600,
               parity=serial.PARITY_NONE,
               stopbits=serial.STOPBITS_ONE,
               bytesize=serial.EIGHTBITS,
               timeout=1)

###routine to write a string to PSI board
def write_to_PSI(sendCommand):
   print('sending...', sendCommand) # uncomment for debugging
   ser.write((sendCommand).encode('utf-8')) #removed CR LF as PSI board doesnt seem to need them`

### routine to read a line (up to CR LF) from PSI board
def listen_to_PSI():
    ser_bytes = ser.readline()
    decoded_bytes = ser_bytes[0:len(ser_bytes)-2].decode('utf-8')
    print(decoded_bytes) # uncomment for debugging
    return decoded_bytes

### routine to turn debug mode ON if OFF 
def turnDebugON():
    print("turning on debug")
    write_to_PSI('o')
    output = listen_to_PSI()
    print("debug on:", output)
    goofything = ser.read() # just read a character to get rid of the '>'
    time.sleep(0.1)
    if output == 'Debug Off':
        print("Debug was already on... turning it back on")
        write_to_PSI('o')
        output = listen_to_PSI()
        print("debug on:", output)
        goofything = ser.read() # just read a character to get rid of the '>'

### routine to write contents to f flash page
### ASSUMES PSI is in Debug Mode ON
def writeFlashPage(page,value):
    print("opening page", page)
    write_to_PSI('F')
    output = listen_to_PSI()
    print(output)
    time.sleep(.5)
    if output == 'Page To Write:':
        print('pick page')
        write_to_PSI(page)
        time.sleep(1)
        output = listen_to_PSI()
        output = listen_to_PSI()
        print(output)
        if output == 'Enter New Value:':
                write_to_PSI(value)
                time.sleep(.5)
                output = listen_to_PSI()
                output = listen_to_PSI()
                print(output)
                
                
 


##### here is where the main loop starts
def main():
    initialize_serialport()
    ser.reset_input_buffer()
    turnDebugON()
    time.sleep(0.5) # just to let the PSI board catch its breath
    writeFlashPage('09',sn)
    time.sleep(1)
    write_to_PSI('o')
    ser.close()




# python bit to figure how who started this
if __name__ == "__main__":
    main()

